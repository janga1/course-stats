# Course: Statistical Methods

Created at Vrije Universiteit Amsterdam

## Description

A series of r-markdown notebooks in which data (see `/data`) is analyzed.

## Note

All data files (see `/data`) property of Vrije Universiteit Amsterdam.
